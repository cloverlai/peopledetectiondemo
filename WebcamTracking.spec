# -*- mode: python -*-

block_cipher = None
datas=[ ( './faster_rcnn_inception_v2_coco_2018_01_28', 'faster_rcnn_inception_v2_coco_2018_01_28' )]

a = Analysis(['WebcamTracking.py'],
             pathex=['/Users/thao/PycharmProjects/PeopleDetection'],
             binaries=[],
             datas=datas,
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='WebcamTracking',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='WebcamTracking')
app = BUNDLE(exe,
         name='HumanTracking.app',
         icon=None,
         bundle_identifier=None)
