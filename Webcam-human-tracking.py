import dlib
import sys
import os

import imutils
import numpy as np
import tensorflow as tf
import cv2
import time
from Configure import Configure


import pyparsing
import packaging
import packaging.version
import packaging.specifiers
import packaging.requirements
from imutils.video import FPS

from pyimagesearch.centroidtracker import CentroidTracker
from pyimagesearch.trackableobject import TrackableObject




class DetectorAPI:
    def __init__(self, path_to_ckpt):
        self.path_to_ckpt = resource_path(path_to_ckpt)
        self.detection_graph = tf.Graph()
        with self.detection_graph.as_default():
            od_graph_def = tf.GraphDef()
            with tf.gfile.GFile(self.path_to_ckpt, 'rb') as fid:
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name='')

        self.default_graph = self.detection_graph.as_default()
        self.sess = tf.Session(graph=self.detection_graph)

        # Definite input and output Tensors for detection_graph
        self.image_tensor = self.detection_graph.get_tensor_by_name('image_tensor:0')
        # Each box represents a part of the image where a particular object was detected.
        self.detection_boxes = self.detection_graph.get_tensor_by_name('detection_boxes:0')
        # Each score represent how level of confidence for each of the objects.
        # Score is shown on the result image, together with the class label.
        self.detection_scores = self.detection_graph.get_tensor_by_name('detection_scores:0')
        self.detection_classes = self.detection_graph.get_tensor_by_name('detection_classes:0')
        self.num_detections = self.detection_graph.get_tensor_by_name('num_detections:0')


    def processFrame(self, image):
        # Expand dimensions since the trained_model expects images to have shape: [1, None, None, 3]
        image_np_expanded = np.expand_dims(image, axis=0)
        # Actual detection.
        start_time = time.time()
        (boxes, scores, classes, num) = self.sess.run(
            [self.detection_boxes, self.detection_scores, self.detection_classes, self.num_detections],
            feed_dict={self.image_tensor: image_np_expanded})
        end_time = time.time()
        #
        # print("Elapsed Time:", end_time - start_time)

        im_height, im_width, _ = image.shape
        boxes_list = [None for i in range(boxes.shape[1])]

        for i in range(boxes.shape[1]):
            boxes_list[i] = (int(boxes[0, i, 0] * im_height),
                             int(boxes[0, i, 1] * im_width),
                             int(boxes[0, i, 2] * im_height),
                             int(boxes[0, i, 3] * im_width))

        return boxes_list, scores[0].tolist(), [int(x) for x in classes[0].tolist()], int(num[0])

    def close(self):
        self.sess.close()

def background_subtraction(previous_frame, frame_resized_grayscale, min_area):
    """
    This function returns 1 for the frames in which the area
    after subtraction with previous frame is greater than minimum area
    defined.
    Thus expensive computation of human detection face detection
    and face recognition is not done on all the frames.
    Only the frames undergoing significant amount of change (which is controlled min_area)
    are processed for detection and recognition.
    """
    frameDelta = cv2.absdiff(previous_frame, frame_resized_grayscale)
    thresh = cv2.threshold(frameDelta, 25, 255, cv2.THRESH_BINARY)[1]
    thresh = cv2.dilate(thresh, None, iterations=2)
    im2, cnts, hierarchy = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    temp = 0
    for c in cnts:
        # if the contour is too small, ignore it
        if cv2.contourArea(c) > min_area:
            temp = 1
    return temp



# Define function to import external files when using PyInstaller.
def resource_path(relative_path):
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)


if __name__ == "__main__":

    # load config file


    config = Configure(resource_path("config.ini"))
    # total_number = config.
    # default
    model_path = './ssd_inception_v2_coco_2017_11_17/frozen_inference_graph.pb'
    model = config.model
    if model == 1:
        model_path = './faster_rcnn_inception_v2_coco_2018_01_28/frozen_inference_graph.pb'
    odapi = DetectorAPI(path_to_ckpt=model_path)
    threshold = 0.7
    W = None
    H = None
    WindowName = "Human Counting"
    writer = None
    trackableObjects = {}
    if not config.webcamUrl == "NONE":
        cap = cv2.VideoCapture(config.webcamUrl)
    else:
        cap = cv2.VideoCapture(0)
    totalLeft = 0
    totalRight = 0
    if cap.isOpened():
        fps = FPS().start()
        r, img = cap.read()
        (H, W) = img.shape[:2]

        ct = CentroidTracker(maxDisappeared=15, maxDistance=50)
        frame_resized = imutils.resize(img, width=min(800, img.shape[1]))
        frame_resized_grayscale = cv2.cvtColor(frame_resized, cv2.COLOR_BGR2GRAY)
        min_area = (3000 / 800) * frame_resized.shape[1]

        # out = cv2.VideoWriter('outpy.avi', cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), 10, (W,H))
        if config.outputVideoFolder is not None and writer is None:
            fourcc = cv2.VideoWriter_fourcc('M', 'J', 'P', 'G')
            directory = resource_path(config.outputVideoFolder)
            videoFile = directory + "/output.avi"
            if not os.path.exists(directory):
                os.makedirs(directory)
            writer = cv2.VideoWriter(videoFile, fourcc, 10,
                                     (W, H))

        while True:
            status = "Waiting"
            trackers = []
            rects = []

            previous_frame = frame_resized_grayscale
            r, img = cap.read()
            if W is None or H is None:
                (H, W) = img.shape[:2]
            if not r:
                break

            frame_resized = imutils.resize(img, width=min(800, img.shape[1]))
            frame_resized_grayscale = cv2.cvtColor(frame_resized, cv2.COLOR_BGR2GRAY)
            temp = background_subtraction(previous_frame, frame_resized_grayscale, min_area)

            if temp == 1:
                boxes, scores, classes, num = odapi.processFrame(img)
                # Visualization of the results of a detection.
                _count = 0
                for i in range(len(boxes)):
                    # Class 1 represents human
                    if classes[i] == 1 and scores[i] > threshold:
                        _count += 1
                        box = boxes[i]
                        cv2.rectangle(img, (box[1], box[0]), (box[3], box[2]), (255, 0, 0), 2)
                        rgb = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
                        tracker = dlib.correlation_tracker()
                        rect = dlib.rectangle(box[1], box[0], box[3], box[2])
                        tracker.start_track(rgb, rect)
                        trackers.append(tracker)

                for tracker in trackers:
                    # set the status of our system to be 'tracking' rather
                    # than 'waiting' or 'detecting'
                    status = "Tracking"

                    # update the tracker and grab the updated position
                    tracker.update(rgb)
                    pos = tracker.get_position()

                    # unpack the position object
                    startX = int(pos.left())
                    startY = int(pos.top())
                    endX = int(pos.right())
                    endY = int(pos.bottom())

                    # add the bounding box coordinates to the rectangles list
                    rects.append((startX, startY, endX, endY))




                    # use the centroid tracker to associate the (1) old object
                    # centroids with (2) the newly computed object centroids
                    objects = ct.update(rects)
                    # loop over the tracked objects

                    for (objectID, centroid) in objects.items():
                        # check to see if a trackable object exists for the current
                        # object ID

                        to = trackableObjects.get(objectID, None)

                        # if there is no existing trackable object, create one
                        if to is None:
                            to = TrackableObject(objectID, centroid)
                        # otherwise, there is a trackable object so we can utilize it
                        # to determine direction
                        else:
                            # the difference between the y-coordinate of the *current*
                            # centroid and the mean of *previous* centroids will tell
                            # us in which direction the object is moving (negative for
                            # 'up' and positive for 'down')
                            x = [c[0] for c in to.centroids]
                            direction = centroid[0] - np.mean(x)
                            to.centroids.append(centroid)

                            # check to see if the object has been counted or not
                            if not to.counted:
                                # if the direction is negative (indicating the object
                                # is moving up) AND the centroid is above the center
                                # line, count the object
                                if direction < 0 and centroid[0] < W // 2:
                                    totalRight += 1
                                    to.counted = True

                                # if the direction is positive (indicating the object
                                # is moving down) AND the centroid is below the
                                # center line, count the object
                                elif direction > 0 and centroid[0] > W // 2:
                                    totalLeft += 1
                                    to.counted = True

                        # store the trackable object in our dictionary
                        trackableObjects[objectID] = to

                        # draw both the ID of the object and the centroid of the
                        # object on the output frame
                        text = "ID {}".format(objectID)
                        cv2.putText(img, text, (centroid[0] - 10, centroid[1] - 10),
                                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
                        cv2.circle(img, (centroid[0], centroid[1]), 4, (0, 255, 0), -1)

            # construct a tuple of information we will be displaying on the
            # frame
            cv2.line(img, (W // 2, 0), (W // 2, H), (0, 255, 255), 2)
            info = [
                ("Right", totalRight),
                ("Left", totalLeft),
                # ("Status", status),
            ]

            # loop over the info tuples and draw them on our frame
            for (i, (k, v)) in enumerate(info):
                text = "{}: {}".format(k, v)
                cv2.putText(img, text, (10, H - ((i * 20) + 20)),
                            cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 0, 255), 2)
            if writer is not None:
                writer.write(img)

            # out.write(img)
            # show the output frame
            cv2.imshow(WindowName, img)
            key = cv2.waitKey(1)
            if key & 0xFF == ord('q'):
              break


        print("exit")
        cap.release()
        odapi.close()
        cv2.destroyAllWindows()
        #just to fix the bug window not close
        cv2.waitKey(0)


    else:
        text = "Camera not opened yet"
        blankimg = np.zeros((300, 400, 3), np.uint8)
        cv2.putText(blankimg, text, (50, 100),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 0, 255), 2)
        cv2.imshow(WindowName, blankimg)
        cv2.waitKey(0)

    key = cv2.waitKey(1)
    if key & 0xFF == ord('q'):
        cap.release()
        odapi.close()
        cv2.destroyAllWindows()
        # just to fix the bug window not close
        cv2.waitKey(1)






