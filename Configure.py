import configparser


class Configure:
    maxDisappeared = 0
    maxDistance = 0
    outputVideoFolder = ''
    webcamUrl = ''
    model = 0

    def __init__(self, path_to_config):
        config = configparser.ConfigParser()
        config.sections()
        config.read(path_to_config)
        config.sections()
        configInfo = config['DEFAULT']
        self.maxDisappeared = configInfo['MaxDisappeared']
        self.maxDistance = configInfo['MaxDistance']
        self.outputVideoFolder = configInfo['OutputVideo']
        self.webcamUrl = configInfo['WebcamUrl']
        self.model = configInfo['Model']
